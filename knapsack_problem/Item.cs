﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knapsack_problem
{
    public class Item:IComparable
    {
       public int id { set; get; }
       public int weight { set; get; }
       public int value { set; get; }

        public override string ToString()
        {
            return id + "\t" + weight + "\t" + value;
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;
            Item otherItem = obj as Item;
            if (otherItem != null) return ((double)otherItem.value / otherItem.weight).CompareTo((double)this.value / this.weight);
               else throw new ArgumentException("Object is not a Item");
        }
    }

}
