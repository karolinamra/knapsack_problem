﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace knapsack_problem
{
    public partial class Form1 : Form
    {
        public List<Item> listOfItems = new List<Item>();
        public Dictionary<string, IAlgorithm> Algotithm = new Dictionary<string, IAlgorithm>();

        public Form1()
        {
            InitializeComponent();
            Algotithm.Add("Greedy Algorithm", new GreedyAlgorithm());
            int i = 0;
            foreach (var pair in Algotithm)
            {
                groupBox1.Controls[i].Text = pair.Key;
                i++;
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            ReadFile read = new ReadFile();

            openFileDialog1.Filter = "csv files (*.csv)|*.csv";

            if (openFileDialog1.ShowDialog() != DialogResult.OK) return;
            try
            {
                if ((myStream = openFileDialog1.OpenFile()) == null) return;
                using (myStream)
                {
                    textBox1.Text = openFileDialog1.FileName;
                    listOfItems = read.ReadFromFile(openFileDialog1.FileName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int allWeight = 0;
            int allValue = 0;
            output.Text = "List of pick items: \r\nID     WEIGHT     VALUE\r\n";
            foreach (RadioButton id in groupBox1.Controls) if (id.Checked)
                foreach (var item in Algotithm[id.Text.ToString()].DoAlgorithm(numericUpDown1.Value, listOfItems))
                {
                    output.Text += item + "\r\n";
                    allWeight += item.weight;
                    allValue += item.value;
                }
            output.Text += "\n Total value: " + allValue;
            output.Text += "\n Total weight: " + allWeight;
        }
    }
}
