﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace knapsack_problem
{
    class GreedyAlgorithm :IAlgorithm
    {
        public List<Item> DoAlgorithm(decimal weightBackpack, List<Item> listOfItems)
        {
            List<Item> chooseItems = new List<Item>();
            decimal weight = 0;
            int i = 0;

            listOfItems.Sort();
            foreach (var t in listOfItems.Where(t => weight + t.weight <= weightBackpack))
            {
                weight += t.weight;
                chooseItems.Add(t);
            }
            return chooseItems;
        }
    }
}
