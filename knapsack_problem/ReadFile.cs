﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace knapsack_problem
{
    class ReadFile
    {

        public List<Item> ReadFromFile(string fileName)
        {
            List<Item> listofItems= new List<Item>();

            try
            {
                using (StreamReader file= new StreamReader(fileName))
                {
                    string line;
                    int counterLines = 0;
                    while ((line = file.ReadLine()) != null)
                    {
                        var tab = line.Split(';');
                       if(counterLines>0)
                        listofItems.Add(new Item(){id=int.Parse(tab[0]),weight = int.Parse(tab[1]),value=int.Parse(tab[2])});
                        counterLines++;
                    }
                    file.Close();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error!" + e.Message, "The file could not be read", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }          
            return listofItems;
        }
    }
}
