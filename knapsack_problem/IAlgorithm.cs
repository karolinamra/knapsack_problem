﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace knapsack_problem
{
    public interface IAlgorithm
    {
        List<Item> DoAlgorithm(decimal weightBackpack, List<Item> listOfItems);
    }
}
